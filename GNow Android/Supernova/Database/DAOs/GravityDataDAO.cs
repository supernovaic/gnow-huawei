﻿using SQLite;
using Supernova.Database.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Supernova.Database.DAO
{
    class GravityDataDAO : DatabaseManagement
    {
        static SQLiteAsyncConnection con;
        public GravityDataDAO()
        {
            con = ConnectionDb();
        }

        public static void Delete(int id)
        {

        }
        public async static void Insert(GravityData obj)
        {
            new GravityDataDAO();
            await con.InsertAsync(obj);
        }

        public async static void Update(GravityData obj)
        {
            new GravityDataDAO();
            await con.UpdateAsync(obj);
        }
        public async Task<List<GravityData>> GetAllData(string id = "")
        {
            string query = "select * from GravityData";
            if (id != null)
            {
                query += " where IdGravity=" + id;
            }

            return await con.QueryAsync<GravityData>(query);
        }
        public async Task<List<GravityData>> GetAllData()
        {
            return await con.QueryAsync<GravityData>("SELECT IdGravity, round(Latitude,6) latitude, round(longitude,6) Longitude, Altitude, round(Gravity,6) Gravity FROM GravityData ORDER BY registered DESC");
        }
        public async Task<int> GetSpecificGravity(string lat, string lng, string alt)
        {
            return (await con.QueryAsync<GravityData>(string.Format("select * from GravityData where Latitude={0} and Longitude={1} and Altitude={2}", lat, lng, alt))).Count;
        }
    }
}
