using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Supernova.Database.DAO;
using Supernova.Database.Entities;
using Supernova.Enums;
using Supernova.Model.Generic;
using System;
using System.Collections.Generic;
using AuditApp.Android;
using AndroidX.DrawerLayout.Widget;
using AndroidX.Core.View;
using static Android.Widget.AdapterView;
using AndroidX.AppCompat.App;
using Com.Github.Florent37.Bubbletab;
using GNow_Android.Adapters;
using AndroidX.ViewPager2.Widget;
using Huawei.Hms.Ads;
using Huawei.Hms.Ads.Banner;
using AndroidX.Preference;
using Google.Android.Material.NavigationRail;

namespace GNow_Android
{
    [Android.App.Activity(Label = "Gravity Now!", MainLauncher = true, Icon = "@drawable/ic_icon")]
    public partial class MainActivity : AppCompatActivity
    {
        private const string NASA_URL = "https://2014.spaceappschallenge.org/awards/#globalawards";

        public DrawerLayout MDrawerLayout { get; set; }
        public ListView MDrawerList { get; set; }
        public TextView TxtCurrentSeekHome { get; set; }
        public TextView TxtCurrentSeekCalc { get; set; }
        public TextView LblOurEmail { get; set; }
        public ImageView ImgSpaceApps { get; set; }
        public Switch BtnDisableLocation { get; set; }
        public SeekBar SeekBarHome { get; set; }
        public SeekBar SeekBarCalc { get; set; }
        public Spinner SpinnerUnits { get; set; }
        public AlertDialog ProfileDialog { get; set; }
        public Profile Profile { get; set; }
        public ActionBar MainActionBar { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetTheme(Resource.Style.Theme_GNowTheme_Material);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            CheckDB();

            HwAds.Init(this);

            FindViewById<BannerView>(Resource.Id.adView).LoadAd(new AdParam.Builder().Build());

/*#if DEBUG
            FindViewById<BannerView>(Resource.Id.adView).Visibility = ViewStates.Gone;
#endif*/

            MDrawerList = FindViewById<ListView>(Resource.Id.navList);
            MDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            MDrawerLayout.AddDrawerListener(new MainActionBarDrawerToggle(this, MDrawerLayout, Resource.String.ApplicationName, Resource.String.ApplicationName));

            MainActionBar = SupportActionBar;

            MainActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            MainActionBar.SetDisplayHomeAsUpEnabled(true);

            if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Portrait)
            {
            var bubbleTab = FindViewById<BubbleTab>(Resource.Id.bubbleTab);
            var viewPager = FindViewById<ViewPager2>(Resource.Id.viewPager);

            viewPager.Adapter = new MainAdapter(this);

                viewPager.RegisterOnPageChangeCallback(new OnPageChangeCallBack(viewPager));
            bubbleTab.SetupWithViewPager(viewPager);
            }
            else
            {
                var sideTab = FindViewById<NavigationRailView>(Resource.Id.main_navigation);

                sideTab.ItemSelected += SideTab_ItemSelected;

                sideTab.SelectedItemId = Resource.Id.menu_home;
            }

            AddDrawerItems();

            Rating();
            Xamarin.Essentials.Platform.Init(this, bundle);
        }

        private void SideTab_ItemSelected(object sender, Google.Android.Material.Navigation.NavigationBarView.ItemSelectedEventArgs e)
        {
            LoadFragment(e.P0.ItemId);
        }

        private void LoadFragment(int id)
        {
            AndroidX.Fragment.App.Fragment fragment = null;
            switch (id)
            {
                case Resource.Id.menu_home:
                    fragment = HomeFragment.NewInstance();
                    break;
                case Resource.Id.menu_calc:
                    fragment = CalcFragment.NewInstance();
                    break;
                case Resource.Id.menu_compare:
                    fragment = ComparisonFragment.NewInstance();
                    break;
                case Resource.Id.menu_search:
                    fragment = OSMSearchFragment.NewInstance();
                    break;
                case Resource.Id.menu_map:
                    fragment = GNowPreview.NewInstance();
                    break;
            }

            if (fragment == null)
            {
                return;
            }

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame_data, fragment)
                .Commit();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main_menu, menu);

            return true;
        }
        private void ImgSpaceApps_Click(object sender, EventArgs e)
        {
            var builder = new AndroidX.Browser.CustomTabs.CustomTabsIntent.Builder();

            var customTabsIntent = builder.Build();

            customTabsIntent.LaunchUrl(this, Android.Net.Uri.Parse(NASA_URL));
            //base.StartActivity(new Intent(Intent.ActionView, Android.Net.Uri.Parse("https://2014.spaceappschallenge.org/awards/#globalawards")));
        }

        private void Rating()
        {
            try
            {
                //You'll have to open, kill the app, then open it again to see the prompt since we are setting 'UsesUntilPrompt' = 2
                AndroidPlaystoreAudit.Instance.TimeUntilPrompt = new TimeSpan(0, 0, 10);
                AndroidPlaystoreAudit.Instance.UsesUntilPrompt = 3;
                AndroidPlaystoreAudit.Instance.PromptTitle = GetString(Resource.String.LblTitleReview);
                AndroidPlaystoreAudit.Instance.ReviewAppStoreButtonText = GetString(Resource.String.LblReviewApp);
                AndroidPlaystoreAudit.Instance.DontRemindButtonText = GetString(Resource.String.LblDontReview);
                AndroidPlaystoreAudit.Instance.RemindLaterButtonText = GetString(Resource.String.LblRemindMeOneWeek);
                AndroidPlaystoreAudit.Instance.RemindLaterTimeToWait = new TimeSpan(168, 0, 0);

                AndroidPlaystoreAudit.Instance.AppStoreNotFound += (sender, e) =>
                {
                    Toast.MakeText(this, GetString(Resource.String.noStore), ToastLength.Long).Show();
                };
                AndroidPlaystoreAudit.Instance.Run(this);
            }
            catch { }
        }

        public override void OnBackPressed()
        {
            if (MDrawerLayout.IsDrawerOpen(GravityCompat.Start))
            { //replace this with actual function which returns if the drawer is open
                MDrawerLayout.CloseDrawers();     // replace this with actual function which closes drawer
            }
            else
            {
                base.OnBackPressed();
            }
        }

        private async void CheckDB()
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(this);

            var firstTime = prefs.GetString("FirstRun", "");

            if (string.IsNullOrEmpty(firstTime))
            {
                Supernova.Database.DatabaseManagement.CopyDataBase(this);
                Profile = await Supernova.Database.DatabaseManagement.AddProfile();

                ISharedPreferencesEditor editor = prefs.Edit();
                editor.PutString("FirstRun", DateTime.Now.ToString("yyyy-MMM-dd"));
                editor.Apply();
            }
            else
            {
                Profile = await new ProfileDAO().GetProfile();
            }
        }

        private void AddDrawerItems()
        {
            MDrawerList.Adapter = new DrawerAdapter(new List<DrawerData>
            {
                new DrawerData() { name = GetString(Resource.String.Settings), image = GetDrawable(Resource.Drawable.cog) },
                new DrawerData() { name = GetString(Resource.String.WhatIsGraivity), image = GetDrawable(Resource.Drawable.earth) },
                new DrawerData() { name = GetString(Resource.String.LblFiveStars), image = GetDrawable(Resource.Drawable.star) },
                new DrawerData() { name = GetString(Resource.String.AboutUs), image = GetDrawable(Resource.Drawable.information) }
            }); ;

            MDrawerList.ItemClick += (object sender, ItemClickEventArgs e) =>
            {
                switch (e.Position)
                {
                    case 0:
                        CreateSettingsDialog();
                        ProfileDialog.Show();
                        break;
                    case 1:
                        StartActivity(new Intent(this, typeof(Help)));
                        break;
                    case 2:
                        DisplayMarketStore();
                        break;
                    case 3:
                        ShowAboutUs();
                        break;
                }

                CheckDrawerStatus();
            };
        }

        private void ShowAboutUs()
        {
            using View dialogView = base.LayoutInflater.Inflate(Resource.Layout.About, null);
            var dialog = new AlertDialog.Builder(this, Resource.Style.Theme_GNowAlertTheme);
            dialog.SetView(dialogView);
            dialog.SetTitle(Resource.String.AboutUs);
            ImgSpaceApps = dialogView.FindViewById<ImageView>(Resource.Id.imgSpaceApps);
            LblOurEmail = dialogView.FindViewById<TextView>(Resource.Id.lblOurEmail);
            LblOurEmail.MovementMethod = Android.Text.Method.LinkMovementMethod.Instance;
            ImgSpaceApps.Click += ImgSpaceApps_Click;
            dialog.Show();
        }

        private void DisplayMarketStore()
        {
            try
            {
                StartActivity(new Intent(Intent.ActionView, Android.Net.Uri.Parse($"market://details?id={PackageName}")));
            }
            catch (ActivityNotFoundException)
            {
                Toast.MakeText(this, GetString(Resource.String.noStore), ToastLength.Long).Show();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void CreateSettingsDialog()
        {
            using View dialogView = base.LayoutInflater.Inflate(Resource.Layout.Settings, null);
            var dialog = new AlertDialog.Builder(this, Resource.Style.Theme_GNowAlertTheme_Material);
            dialog.SetView(dialogView);
            BtnDisableLocation = dialogView.FindViewById<Switch>(Resource.Id.btnDisableLocation);
            SeekBarHome = dialogView.FindViewById<SeekBar>(Resource.Id.seekBarHome);
            SeekBarCalc = dialogView.FindViewById<SeekBar>(Resource.Id.seekBarCalc);
            SpinnerUnits = dialogView.FindViewById<Spinner>(Resource.Id.spinnerUnits);
            TxtCurrentSeekHome = dialogView.FindViewById<TextView>(Resource.Id.txtCurrentSeekHome);
            TxtCurrentSeekCalc = dialogView.FindViewById<TextView>(Resource.Id.txtCurrentSeekCalc);

            SeekBarHome.ProgressChanged += SeekBarHome_ProgressChanged;
            SeekBarCalc.ProgressChanged += SeekBarCalc_ProgressChanged;

            SeekBarCalc.Progress = Profile.CalcDecimal;
            SeekBarHome.Progress = Profile.NDecimal;

            TxtCurrentSeekHome.Text = Profile.NDecimal.ToString();
            TxtCurrentSeekCalc.Text = Profile.CalcDecimal.ToString();

            var adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerItem, new List<string>
                {
                    GetString(Resource.String.hintUnitsMetric),
                    GetString(Resource.String.hintUnitsEmperial)
                });

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

            SpinnerUnits.Adapter = adapter;
            SpinnerUnits.SetSelection((int)Profile.SelectedUnits);

            BtnDisableLocation.Checked = Profile.Sync;

            dialog.SetTitle(Resource.String.Settings);

            dialog.SetPositiveButton(Resource.String.btnSave, BtnSave_Click);
            dialog.SetNegativeButton(Resource.String.btnCancel, (sender, e) => { });

            ProfileDialog = dialog.Create();
        }

        private void SeekBarCalc_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            TxtCurrentSeekCalc.Text = SeekBarCalc.Progress.ToString();
        }

        private void SeekBarHome_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            TxtCurrentSeekHome.Text = SeekBarHome.Progress.ToString();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Profile.CalcDecimal = SeekBarCalc.Progress;
            Profile.NDecimal = SeekBarHome.Progress;
            Profile.Sync = BtnDisableLocation.Checked;
            Profile.SelectedUnits = (Units)SpinnerUnits.SelectedItemPosition;

            ProfileDAO.Update(Profile);

            Toast.MakeText(this, GetString(Resource.String.lblSave), ToastLength.Short).Show();

            ProfileDialog.Dismiss();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    if (MDrawerLayout.IsDrawerOpen(GravityCompat.Start))
                    {
                        MDrawerLayout.CloseDrawers();
                    }
                    else
                    {
                        MDrawerLayout.OpenDrawer(GravityCompat.Start);
                    }

                    return true;
                case Resource.Id.action_help:
                    HelpModal();
                    return true;
                default:
                    return false;
            }
        }

        private void HelpModal()
        {
            View dialogView = base.LayoutInflater.Inflate(Resource.Layout.HelpFormulaModal, null);
            using var Dialog = new Android.App.AlertDialog.Builder(this);
            Dialog.SetView(dialogView);
            Dialog.Show();
        }
        private void CheckDrawerStatus()
        {
            if (MDrawerLayout.IsDrawerOpen(GravityCompat.Start))
            {
                MainActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
                MDrawerLayout.CloseDrawers();
            }
        }
    }
}
