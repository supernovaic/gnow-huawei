﻿using Android.Locations;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.SwipeRefreshLayout.Widget;
using Google.Android.Material.FloatingActionButton;
using Newtonsoft.Json;
using Supernova.Database.DAO;
using Supernova.Database.Entities;
using Supernova.Enums;
using Supernova.Model.Generic;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Essentials;
using static Xamarin.Essentials.Permissions;

namespace GNow_Android
{
    public class HomeFragment : AndroidX.Fragment.App.Fragment
    {
        private const string GEO_NAMES = "http://api.geonames.org/srtm3JSON?lat={0}&lng={1}&username=fanmixco";
        private const string WAITING = "…";
        private const string M_S_2 = " m/s²";
        private const string FT_S_2 = " ft/s²";
        private const string M = "m";
        private const string FT = "ft";

        private SwipeRefreshLayout rLayout;
        private TextView lblGravity;
        private TextView lblAltitude;
        private TextView lblLongitude;
        private TextView lblLatitude;
        private TextView lblGravityUnits;
        private TableLayout tableAltLatLng;
        private FloatingActionButton btnSwap;
        private ProfileDAO pDAO;

        private Profile profile;
        private bool isGPSEnabled;

        public static AndroidX.Fragment.App.Fragment NewInstance()
        {
            return new HomeFragment();
        }

        public async Task GetLocationAsync()
        {
            if (profile != null)
            {
                if (!profile.Sync)
                {
                    try
                    {
                        if (isGPSEnabled)
                        {
                            var location = await Geolocation.GetLocationAsync();

                            if (location != null)
                            {
                                await ConfigHome(location);
                            }
                            else
                            {
                                await GetLocationByIP();
                            }
                        }
                        else
                        {
                            await GetLocationByIP();
                        }
                    }
                    catch
                    {
                        DefaultGravity();
                    }
                }
                else
                {
                    ErrorLocation(GetString(Resource.String.disabledLocation));
                }
            }
            else
            {
                ErrorLocation(GetString(Resource.String.disabledLocation));
            }
        }

        private void ErrorLocation(string error)
        {
            tableAltLatLng.Visibility = ViewStates.Gone;

            lblGravityUnits.Text = ".";

            lblGravity.TextSize = 16;

            lblGravity.Text = error;
        }

        private async Task GetLocationByIP()
        {
            var current = Connectivity.NetworkAccess;

            if (current == Xamarin.Essentials.NetworkAccess.Internet)
            {
                var locationIP = await Supernova.WebServices.IPStack.GetLocation();

                if (locationIP.Item1 == -404 && locationIP.Item2 == -404 || locationIP.Item1 == 0 && locationIP.Item2 == 0)
                {
                    DefaultGravity();
                }
                else
                {
                    await ConfigHome(new Xamarin.Essentials.Location(locationIP.Item1, locationIP.Item2));
                }
            }
            else
            {
                DefaultGravity();
            }
        }

        private void DefaultGravity()
        {
            lblGravity.TextSize = 56;
            lblLatitude.Text = "0°";
            lblLongitude.Text = "0°";
            tableAltLatLng.Visibility = ViewStates.Visible;

            if (profile.SelectedUnits == Units.Meters)
            {
                lblGravity.Text = "9.798";
                lblAltitude.Text = $"0{M}";
                lblGravityUnits.Text = M_S_2;
            }
            else
            {
                lblGravity.Text = "32.146";
                lblAltitude.Text = $"0{FT}";
                lblGravityUnits.Text = FT_S_2;
            }
        }

        private async Task ConfigHome(Xamarin.Essentials.Location location)
        {
            try
            {
                lblGravity.Text = await GetGravityResult(location.Latitude, location.Longitude, location.Altitude ?? 0, profile.SelectedUnits, profile.NDecimal);
            }
            catch
            {
                tableAltLatLng.Visibility = ViewStates.Gone;

                lblGravityUnits.Text = WAITING;

                lblGravity.TextSize = 16;

                lblGravity.Text = GetString(Resource.String.errGettingLocation);
            }
        }

        public async Task<PermissionStatus> CheckAndRequestPermissionAsync<T>(T permission)
                    where T : BasePermission
        {
            var status = await permission.CheckStatusAsync();
            if (status != PermissionStatus.Granted)
            {
                status = await permission.RequestAsync();
            }

            return status;
        }

        private async Task<int> GetAltitude(double latitude, double longitude)
        {
            try
            {
                using var client = new HttpClient();
                var result = await client.GetAsync(new Uri(string.Format(GEO_NAMES, latitude, longitude)));
                if (result.IsSuccessStatusCode && result.StatusCode == HttpStatusCode.OK)
                {
                    //ok to process
                    var json = await result.Content.ReadAsStringAsync();
                    var status = JsonConvert.DeserializeObject<ResultData>(json);
                    return status.srtm3; ;
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }

        private async Task<string> GetGravityResult(double latitude, double longitude, double altitude, Units units, int Decimals)
        {
            try
            {
                tableAltLatLng.Visibility = ViewStates.Gone;

                lblGravityUnits.Text = WAITING;

                lblGravity.TextSize = 16;

                altitude = (altitude == 0) ? await GetAltitude(latitude, longitude) : altitude;

                var GValue = new Supernova.Core.Gravity(latitude, 0, altitude);

                altitude = (units == Units.Feet) ? Math.Round(GValue.ChangeToFeet(altitude), 0) : altitude;

                double result = GValue.GetGravity();
                if (altitude == 0)
                {
                    return GetString(Resource.String.errAltitude);
                }
                else if (result == 0)
                {
                    return GetString(Resource.String.errLatitude);
                }
                else if (result == 1)
                {
                    return GetString(Resource.String.errAltitudeAbove);
                }
                else if (result == 2)
                {
                    return GetString(Resource.String.errAltitudeBelow);
                }
                else
                {
                    lblGravity.TextSize = 56;

                    lblLatitude.Text = $"{Math.Round(latitude, 5)}°";
                    lblLongitude.Text = $"{Math.Round(longitude, 5)}°";

                    tableAltLatLng.Visibility = ViewStates.Visible;

                    /*var gDao = new GravityDataDAO();

                    int exist = await gDao.GetSpecificGravity(latitude.ToString(), longitude.ToString(), altitude.ToString());

                    if (exist == 0)
                    {
                        GravityDataDAO.Insert(new GravityData() { Altitude = (int)altitude, Gravity = result, Latitude = latitude, Longitude = longitude, Registered = DateTime.Now });
                    }*/

                    if (units == Units.Feet)
                    {
                        lblAltitude.Text = $"{Math.Round(altitude, 0)}{FT}";
                        lblGravityUnits.Text = FT_S_2;
                        return Math.Round(GValue.ChangeToFeet(result), Decimals).ToString();
                    }
                    else
                    {
                        lblAltitude.Text = $"{Math.Round(altitude, 0)}{M}";
                        lblGravityUnits.Text = M_S_2;
                        return Math.Round(result, Decimals).ToString();
                    }
                }
            }
            catch
            {
                lblGravityUnits.Text = WAITING;

                lblGravity.TextSize = 16;

                tableAltLatLng.Visibility = ViewStates.Gone;

                return GetString(Resource.String.errGettingLocation);
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.Home, container, false);

            try
            {
                var providerManager = (LocationManager)Activity.GetSystemService(Android.Content.Context.LocationService);

                isGPSEnabled = providerManager.IsProviderEnabled(LocationManager.GpsProvider);
            }
            catch { }

            pDAO = new ProfileDAO();

            lblGravity = view.FindViewById<TextView>(Resource.Id.lblGravity);
            lblAltitude = view.FindViewById<TextView>(Resource.Id.lblAltitude);
            lblLatitude = view.FindViewById<TextView>(Resource.Id.lblLatitude);
            lblLongitude = view.FindViewById<TextView>(Resource.Id.lblLongitude);
            lblGravityUnits = view.FindViewById<TextView>(Resource.Id.lblGravityUnits);
            tableAltLatLng = view.FindViewById<TableLayout>(Resource.Id.tableAltLatLng);
            btnSwap = view.FindViewById<FloatingActionButton>(Resource.Id.btnSwap);

            rLayout = view.FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout);

            btnSwap.Click += BtnSwap_Click;

            lblGravity.TextSize = 16;
            lblGravity.TextAlignment = TextAlignment.Center;

            GetCurrentLocation();

            rLayout.Refresh += RLayout_Refresh;

            return view;
        }

        private async void RLayout_Refresh(object sender, EventArgs e)
        {
            profile = await pDAO.GetProfile();

            await GetLocationAsync();
            rLayout.Refreshing = false;
        }

        private async void GetCurrentLocation()
        {
            profile = await pDAO.GetProfile();

            await GetLocationAsync();
        }

        private async void BtnSwap_Click(object sender, EventArgs e)
        {
            profile = await pDAO.GetProfile();

            if (lblGravityUnits.Text != WAITING)
            {
                var GValue = new Supernova.Core.Gravity();

                if (lblGravityUnits.Text.Contains(M))
                {
                    lblGravity.Text = Math.Round(GValue.ChangeToFeet(double.Parse(lblGravity.Text)), profile.NDecimal).ToString();
                    lblGravityUnits.Text = FT_S_2;

                    lblAltitude.Text = lblAltitude.Text.Replace(M, "");
                    lblAltitude.Text = $"{Math.Round(GValue.ChangeToFeet(double.Parse(lblAltitude.Text)), 0)}{FT}";
                }
                else
                {
                    lblGravity.Text = Math.Round(GValue.ChangeToMetres(double.Parse(lblGravity.Text)), profile.NDecimal).ToString();
                    lblGravityUnits.Text = M_S_2;

                    lblAltitude.Text = lblAltitude.Text.Replace(FT, "");
                    lblAltitude.Text = $"{Math.Round(GValue.ChangeToMetres(double.Parse(lblAltitude.Text)), 0)}{M}";
                }
            }
        }
    }
}
