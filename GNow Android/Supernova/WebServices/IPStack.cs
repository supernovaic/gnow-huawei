﻿using System;
using System.IO;
using System.Json;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Supernova.WebServices
{
    public class IPStack
    {
        private const string IPIFY_URL = "https://api.ipify.org/?format=json";
        private const string JSON_CONST = "application/json";
        private const string METHOD_GET = "GET";

        private class IPStackValues
        {
            public double latitude { get; set; }
            public double longitude { get; set; }
        }

        private class IpResult
        {
            public string ip { get; set; }
        }

        private async static Task<string> GetLocalIPAddress()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(IPIFY_URL);
            var resultString = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<IpResult>(resultString);

            return result.ip;
        }

        public static async Task<Tuple<double, double>> GetLocation()
        {
            var IP = await GetLocalIPAddress();

            if (!string.IsNullOrEmpty(IP))
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri($"http://api.ipstack.com/${IP}?access_key=e001f2fabacc1684a3140a7eb75448fd"));
                request.ContentType = JSON_CONST;
                request.Method = METHOD_GET;

                using WebResponse response = await request.GetResponseAsync();
                using Stream stream = response.GetResponseStream();

                var responseJson = JsonConvert.DeserializeObject<IPStackValues>((await Task.Run(() => JsonValue.Load(stream))).ToString());

                return new Tuple<double, double>(responseJson.latitude, responseJson.longitude);
            }
            else
            {
                return new Tuple<double, double>(-404, -404);
            }
        }
    }
}