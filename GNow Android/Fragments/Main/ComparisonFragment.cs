﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;
using Google.Android.Material.TextField;
using Java.IO;
using Supernova.Database.DAO;
using Supernova.Database.Entities;
using Supernova.Model.Core;
using Supernova.Model.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Essentials;
using File = Java.IO.File;
using System.Text;

namespace GNow_Android
{
    public partial class ComparisonFragment : AndroidX.Fragment.App.Fragment
    {
        private const string NASA_LINK = "https://solarsystem.nasa.gov/planets/overview/";
        private const string FILE_PROVIDER = "tk.supernova.gnow.fileprovider";
        private const string RED_COLOR = "#F44336";
        private const string GREEN_COLOR = "#43A047";
        private const string BLUE_COLOR = "#2196F3";
        private const string SHARE_OPTION = "text/plain";
        private const string MAIL_TYPE = "text/html";

        private readonly List<string> weightUnits = new() { "kg", "lb" };
        private readonly List<string> gUnits = new() { "m/s²", "ft/s²" };

        private AutoCompleteTextView spinnerWeightUnits;
        private AutoCompleteTextView spinnerGravityUnits;
        private AutoCompleteTextView spinnerFirstCelestial;
        private AutoCompleteTextView spinnerSecondCelestial;
        private TextView lblDFirstCelestial;
        private TextView lblDSecondCelestial;
        private TextView lblGFirstCelestial;
        private TextView lblGSecondCelestial;
        private TextView lblGDescription;
        private TextView lblDWeight;
        private TextInputEditText txtWeight;
        private ImageView imgFirstCelestial;
        private ImageView imgSecondCelestial;
        private ImageView imgWeight;
        private Button btnShare;

        private Profile profile;

        private List<string> planets;
        private List<DrawerData> compareAllList;
        private List<CelestialDataLight> cLight;
        private List<CelestialDataExpanded> cExpanded;

        private string secondWeight = "";
        private string wikiURL;

        public static AndroidX.Fragment.App.Fragment NewInstance()
        {
            return new ComparisonFragment();
        }

        private File CreateDirFile(string fileName)
        {
            var comparisonsPath = new File(FileSystem.AppDataDirectory, "comparisons");
            comparisonsPath.Mkdir();
            var file = new File(comparisonsPath, fileName);

            if (file.Exists())
            {
                file.Delete();
            }

            file.CreateNewFile();
            return file;
        }

        private List<DrawerData> GetPlanetsData()
        {
            return new List<DrawerData>()
            {
                new DrawerData() {
                    name = GetString(Resource.String.itemSun),
                    image = GetCelestialObjectDrawable(Resource.Drawable.sun),
                    url = GetString(Resource.String.itemWikiSun)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemMercury),
                    image = GetCelestialObjectDrawable(Resource.Drawable.mercury),
                    url = GetString(Resource.String.itemWikiMercury)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemVenus),
                    image = GetCelestialObjectDrawable(Resource.Drawable.venus),
                    url = GetString(Resource.String.itemWikiVenus)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemEarth),
                    image = GetCelestialObjectDrawable(Resource.Drawable.earthP),
                    url = GetString(Resource.String.itemWikiEarth)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemMoon),
                    image = GetCelestialObjectDrawable(Resource.Drawable.moon),
                    url = GetString(Resource.String.itemWikiMoon)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemMars),
                    image = GetCelestialObjectDrawable(Resource.Drawable.mars),
                    url = GetString(Resource.String.itemWikiMars)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemPhobos),
                    image = GetCelestialObjectDrawable(Resource.Drawable.phobos),
                    url = GetString(Resource.String.itemWikiPhobos)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemDeimos),
                    image = GetCelestialObjectDrawable(Resource.Drawable.deimos),
                    url = GetString(Resource.String.itemWikiDeimos)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemCeres),
                    image = GetCelestialObjectDrawable(Resource.Drawable.ceres),
                    url = GetString(Resource.String.itemWikiCeres)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemJupiter),
                    image = GetCelestialObjectDrawable(Resource.Drawable.jupiter),
                    url = GetString(Resource.String.itemWikiJupiter)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemIo),
                    image = GetCelestialObjectDrawable(Resource.Drawable.io),
                    url = GetString(Resource.String.itemWikiIo)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemEuropa),
                    image = GetCelestialObjectDrawable(Resource.Drawable.europa),
                    url = GetString(Resource.String.itemWikiEuropa)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemAmalthea),
                    image = GetCelestialObjectDrawable(Resource.Drawable.amalthea),
                    url = GetString(Resource.String.itemWikiAmalthea)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemGanymede),
                    image = GetCelestialObjectDrawable(Resource.Drawable.ganymede),
                    url = GetString(Resource.String.itemWikiGanymede)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemCallisto),
                    image = GetCelestialObjectDrawable(Resource.Drawable.callisto),
                    url = GetString(Resource.String.itemWikiCallisto)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemSaturn),
                    image = GetCelestialObjectDrawable(Resource.Drawable.saturn),
                    url = GetString(Resource.String.itemWikiSaturn)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemDaphnis),
                    image = GetCelestialObjectDrawable(Resource.Drawable.daphnis),
                    url = GetString(Resource.String.itemWikiDaphnis)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemMimas),
                    image = GetCelestialObjectDrawable(Resource.Drawable.mimas),
                    url = GetString(Resource.String.itemWikiMimas)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemEnceladus),
                    image = GetCelestialObjectDrawable(Resource.Drawable.enceladus),
                    url = GetString(Resource.String.itemWikiEnceladus)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemTethys),
                    image = GetCelestialObjectDrawable(Resource.Drawable.tethys),
                    url = GetString(Resource.String.itemWikiTethys)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemDione),
                    image = GetCelestialObjectDrawable(Resource.Drawable.dione),
                    url = GetString(Resource.String.itemWikiDione)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemRhea),
                    image = GetCelestialObjectDrawable(Resource.Drawable.rhea),
                    url = GetString(Resource.String.itemWikiRhea)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemTitan),
                    image = GetCelestialObjectDrawable(Resource.Drawable.titan),
                    url = GetString(Resource.String.itemWikiTitan)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemHelene),
                    image = GetCelestialObjectDrawable(Resource.Drawable.helene),
                    url = GetString(Resource.String.itemWikiHelene)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemPan),
                    image = GetCelestialObjectDrawable(Resource.Drawable.pan),
                    url = GetString(Resource.String.itemWikiPan)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemTelesto),
                    image = GetCelestialObjectDrawable(Resource.Drawable.telesto),
                    url = GetString(Resource.String.itemWikiTelesto)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemHyperion),
                    image = GetCelestialObjectDrawable(Resource.Drawable.hyperion),
                    url = GetString(Resource.String.itemWikiHyperion)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemIapetus),
                    image = GetCelestialObjectDrawable(Resource.Drawable.iapetus),
                    url = GetString(Resource.String.itemWikiIapetus)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemPhoebe),
                    image = GetCelestialObjectDrawable(Resource.Drawable.phoebe),
                    url = GetString(Resource.String.itemWikiPhoebe)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemJanus),
                    image = GetCelestialObjectDrawable(Resource.Drawable.janus),
                    url = GetString(Resource.String.itemWikiJanus)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemPandora),
                    image = GetCelestialObjectDrawable(Resource.Drawable.pandora),
                    url = GetString(Resource.String.itemWikiPandora)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemAtlas),
                    image = GetCelestialObjectDrawable(Resource.Drawable.atlas),
                    url = GetString(Resource.String.itemWikiAtlas)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemUranus),
                    image = GetCelestialObjectDrawable(Resource.Drawable.uranus),
                    url = GetString(Resource.String.itemWikiUranus)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemAriel),
                    image = GetCelestialObjectDrawable(Resource.Drawable.ariel),
                    url = GetString(Resource.String.itemWikiAriel)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemUmbriel),
                    image = GetCelestialObjectDrawable(Resource.Drawable.umbriel),
                    url = GetString(Resource.String.itemWikiUmbriel)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemTitania),
                    image = GetCelestialObjectDrawable(Resource.Drawable.titania),
                    url = GetString(Resource.String.itemWikiTitania)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemOberon),
                    image = GetCelestialObjectDrawable(Resource.Drawable.oberon),
                    url = GetString(Resource.String.itemWikiOberon)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemMiranda),
                    image = GetCelestialObjectDrawable(Resource.Drawable.miranda),
                    url = GetString(Resource.String.itemWikiMiranda)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemNeptune),
                    image = GetCelestialObjectDrawable(Resource.Drawable.neptune),
                    url = GetString(Resource.String.itemWikiNeptune)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemTriton),
                    image = GetCelestialObjectDrawable(Resource.Drawable.triton),
                    url = GetString(Resource.String.itemWikiTriton)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemProteus),
                    image = GetCelestialObjectDrawable(Resource.Drawable.proteus),
                    url = GetString(Resource.String.itemWikiProteus)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemPluto),
                    image = GetCelestialObjectDrawable(Resource.Drawable.pluto),
                    url = GetString(Resource.String.itemWikiPluto)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemCharon),
                    image = GetCelestialObjectDrawable(Resource.Drawable.charon),
                    url = GetString(Resource.String.itemWikiCharon)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemNix),
                    image = GetCelestialObjectDrawable(Resource.Drawable.nix),
                    url = GetString(Resource.String.itemWikiNix)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemEris),
                    image = GetCelestialObjectDrawable(Resource.Drawable.eris),
                    url = GetString(Resource.String.itemWikiEris)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemMakemake),
                    image = GetCelestialObjectDrawable(Resource.Drawable.makemake),
                    url = GetString(Resource.String.itemWikiMakemake)
                },
                new DrawerData() {
                    name = GetString(Resource.String.itemHaumea),
                    image = GetCelestialObjectDrawable(Resource.Drawable.haumea),
                    url = GetString(Resource.String.itemWikiHaumea)
                }
            };
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.Comparison, container, false);

            compareAllList = GetPlanetsData();

            planets = compareAllList.Select(x => x.name).ToList();

            txtWeight = view.FindViewById<TextInputEditText>(Resource.Id.txtWeight);

            lblDFirstCelestial = view.FindViewById<TextView>(Resource.Id.lblDFirstCelestial);
            lblDSecondCelestial = view.FindViewById<TextView>(Resource.Id.lblDSecondCelestial);
            lblGFirstCelestial = view.FindViewById<TextView>(Resource.Id.lblGFirstCelestial);
            lblGSecondCelestial = view.FindViewById<TextView>(Resource.Id.lblGSecondCelestial);
            lblDWeight = view.FindViewById<TextView>(Resource.Id.lblDWeight);
            lblGDescription = view.FindViewById<TextView>(Resource.Id.lblGDescription);

            spinnerFirstCelestial = view.FindViewById<AutoCompleteTextView>(Resource.Id.spinnerFirstCelestial);
            spinnerSecondCelestial = view.FindViewById<AutoCompleteTextView>(Resource.Id.spinnerSecondCelestial);
            spinnerWeightUnits = view.FindViewById<AutoCompleteTextView>(Resource.Id.spinnerWeightUnits);
            spinnerGravityUnits = view.FindViewById<AutoCompleteTextView>(Resource.Id.spinnerUnits);

            imgFirstCelestial = view.FindViewById<ImageView>(Resource.Id.imgFirstCelestial);
            imgFirstCelestial.Click += ImgFirstCelestial_Click;
            imgSecondCelestial = view.FindViewById<ImageView>(Resource.Id.imgSecondCelestial);
            imgSecondCelestial.Click += ImgSecondCelestial_Click;

            imgWeight = view.FindViewById<ImageView>(Resource.Id.imgWeight);

            var btnCompareAll = view.FindViewById<Button>(Resource.Id.btnCompareAll);
            btnShare = view.FindViewById<Button>(Resource.Id.btnShare);

            btnCompareAll.Click += BtnCompareAll_Click;

            btnShare.Click += BtnShare_Click;

            var adapterWeight = new ArrayAdapter<string>(Activity, Resource.Layout.DropdownMenuPopupItem, weightUnits);

            spinnerWeightUnits.Adapter = adapterWeight;

            var adapterGravity = new ArrayAdapter<string>(Activity, Resource.Layout.DropdownMenuPopupItem, gUnits);

            spinnerGravityUnits.Adapter = adapterGravity;
            spinnerGravityUnits.SetText(gUnits[0], false);

            spinnerWeightUnits.SetText(weightUnits[0], false);

            var adapterPlanets1 = new ArrayAdapter<string>(Activity, Resource.Layout.DropdownMenuPopupItem, planets);
            var adapterPlanets2 = new ArrayAdapter<string>(Activity, Resource.Layout.DropdownMenuPopupItem, planets);

            spinnerFirstCelestial.Adapter = adapterPlanets1;
            spinnerFirstCelestial.SetText(GetString(Resource.String.itemEarth), false);

            spinnerSecondCelestial.Adapter = adapterPlanets2;
            spinnerSecondCelestial.SetText(GetString(Resource.String.itemEarth), false);

            spinnerFirstCelestial.ItemClick += (sender, args) =>
            {
                SetPlanet(args.Position, imgFirstCelestial);

                CalculateGravity();

                CalculateWeight();
            };

            spinnerSecondCelestial.ItemClick += (sender, args) =>
            {
                SetPlanet(args.Position, imgSecondCelestial);

                CalculateGravity();

                CalculateWeight();
            };

            spinnerGravityUnits.ItemClick += (sender, args) =>
            {
                SetBasics();
            };

            spinnerWeightUnits.ItemClick += (sender, args) =>
            {
                SetBasics();
            };

            txtWeight.TextChanged += TxtWeight_TextChanged;

            GetProfile();

            wikiURL = $"https://{GetString(Resource.String.wikiURL)}/";

            return view;
        }

        private bool CheckInternetState()
        {
            return Connectivity.NetworkAccess == NetworkAccess.Internet;
        }

        private void ImgFirstCelestial_Click(object sender, EventArgs e)
        {
            int firstCelestialLoc = GetItemPosition(planets, spinnerFirstCelestial.Text);

            OpenURL($"{wikiURL}{compareAllList[firstCelestialLoc].url}");
        }

        private void ImgSecondCelestial_Click(object sender, EventArgs e)
        {
            int secondCelestialLoc = GetItemPosition(planets, spinnerSecondCelestial.Text);

            OpenURL($"{wikiURL}{compareAllList[secondCelestialLoc].url}");
        }

        private bool OpenURL(string url)
        {
            if (CheckInternetState())
            {
                var builder = new AndroidX.Browser.CustomTabs.CustomTabsIntent.Builder();

                var customTabsIntent = builder.Build();

                customTabsIntent.LaunchUrl(Context, Android.Net.Uri.Parse(url));

                return true;
            }

            return false;
        }

        private void BtnShare_Click(object sender, EventArgs e)
        {
            using Intent shareIntent = new(Intent.ActionSend);
            shareIntent.SetType(SHARE_OPTION);
            shareIntent.PutExtra(Intent.ExtraSubject, GetString(Resource.String.lblShareMsgTitle));

            string extra = "";

            int secondCelestialLoc = GetItemPosition(planets, spinnerSecondCelestial.Text);

            if (new int[] { 0, 3, 4 }.Contains(secondCelestialLoc))
            {
                extra = $"{GetString(Resource.String.thePlus)} ";
            }

            shareIntent.PutExtra(Intent.ExtraText, string.Format(GetString(Resource.String.lblShareMsg), secondWeight, $"{extra}{spinnerSecondCelestial.Text}"));

            using var intentCreate = Intent.CreateChooser(shareIntent, GetString(Resource.String.lblShare));
            StartActivity(intentCreate);
        }

        private void BtnCompareAll_Click(object sender, EventArgs e)
        {
            GravityPlanets gInfo = new();

            if (string.IsNullOrEmpty(txtWeight.Text))
            {
                cLight = new List<CelestialDataLight>();
            }
            else
            {
                cExpanded = new List<CelestialDataExpanded>();
            }

            View dialogView = LayoutInflater.Inflate(Resource.Layout.CompareAll, null);
            var Dialog = new AlertDialog.Builder(Context);

            int firstCelestialLoc = GetItemPosition(planets, spinnerFirstCelestial.Text);
            var gravity = new Supernova.Core.Gravity();

            var listView = dialogView.FindViewById<ListView>(Resource.Id.lstCelestialObjects);
            var textObject1 = dialogView.FindViewById<TextView>(Resource.Id.textObject1);
            var imgView = dialogView.FindViewById<ImageView>(Resource.Id.imgCObject);
            var yourWeight = dialogView.FindViewById<TextView>(Resource.Id.yourWeight);

            imgView.SetImageDrawable(compareAllList[firstCelestialLoc].image);

            string firstObject = GetCelestialObject(gInfo.CelestialObjectType(firstCelestialLoc));
            string firstGravity;

            if (spinnerGravityUnits.Text == gUnits[0])
            {
                firstGravity = $"{gInfo.GetGravity(firstCelestialLoc)} {spinnerGravityUnits.Text}";
            }
            else
            {
                firstGravity = $"{Math.Round(gravity.ChangeToFeet(gInfo.GetGravity(firstCelestialLoc)), 3)} {spinnerGravityUnits.Text}";
            }

            textObject1.Text = $"{planets[firstCelestialLoc]}, {firstObject}, {firstGravity}";

            if (string.IsNullOrEmpty(txtWeight.Text))
            {
                cLight.Add(new CelestialDataLight(planets[firstCelestialLoc], firstObject, firstGravity.Replace("?", ""), "N/A"));
            }
            else
            {
                cExpanded.Add(new CelestialDataExpanded(planets[firstCelestialLoc], firstObject, firstGravity.Replace("?", ""), "N/A", $"{txtWeight.Text} {spinnerWeightUnits.Text}"));
            }

            List<DrawerData> currentObjects = GetPlanetsData();
            currentObjects.RemoveAt(firstCelestialLoc);

            bool isNumeric = double.TryParse(txtWeight.Text, out double earthWeight);
            if (isNumeric && earthWeight > 0)
            {
                yourWeight.Visibility = ViewStates.Visible;
                yourWeight.Text = $"{GetString(Resource.String.lblYourWeight)} {txtWeight.Text}{spinnerWeightUnits.Text}";
            }
            else
            {
                yourWeight.Visibility = ViewStates.Gone;
                yourWeight.Text = "";
            }

            GravityPlanets gPlanets = new();

            for (int i = 0; i < currentObjects.Count(); i++)
            {
                var tmpDataLight = new CelestialDataExpanded();

                var secondG = GetItemPosition(planets, currentObjects[i].name);

                tmpDataLight.Name = currentObjects[i].name;

                var gPercentage = Math.Round(gInfo.PercentageGravity(firstCelestialLoc, secondG), 0);
                string gravType;

                switch (gInfo.ComparedGravity(firstCelestialLoc, secondG))
                {
                    case 0:
                        gravType = GetString(Resource.String.gravLower);
                        currentObjects[i].color = GREEN_COLOR;
                        break;
                    default:
                        gravType = GetString(Resource.String.gravGreater);
                        currentObjects[i].color = RED_COLOR;
                        break;
                }

                var gravVal = spinnerGravityUnits.Text == gUnits[0] ? gInfo.GetGravity(secondG) : Math.Round(gravity.ChangeToFeet(gInfo.GetGravity(secondG)), 3);

                tmpDataLight.Type = GetCelestialObject(gInfo.CelestialObjectType(secondG));

                tmpDataLight.Gravity = $"{gravVal} {spinnerGravityUnits.Text}".Replace("²", "ˆ2");

                tmpDataLight.Comparison = string.Format(gravType, gPercentage);

                currentObjects[i].name = $"<b>{currentObjects[i].name}, {GetCelestialObject(gInfo.CelestialObjectType(secondG))}</b><br>{gravVal} {spinnerGravityUnits.Text}, {string.Format(gravType, gPercentage)}";

                if (isNumeric && earthWeight > 0)
                {
                    currentObjects[i].name += $"<br>{GetString(Resource.String.lblYourWeightCO)} {GetNewWeight(gPlanets, double.Parse(txtWeight.Text), firstCelestialLoc, secondG)} {spinnerWeightUnits.Text}";

                    //Add weight in the next update
                    //tmpDataLight.Comparison += $" {GetString(Resource.String.lblYourWeightCO)} {GetNewWeight(gPlanets, double.Parse(txtWeight.Text), firstCelestialLoc, secondG)} {spinnerWeightUnits.Text}";
                }

                if (string.IsNullOrEmpty(txtWeight.Text))
                {
                    cLight.Add(new CelestialDataLight(tmpDataLight.Name, tmpDataLight.Type, tmpDataLight.Gravity, tmpDataLight.Comparison));
                }
                else
                {
                    tmpDataLight.MyWeight = $"{GetNewWeight(gPlanets, double.Parse(txtWeight.Text), firstCelestialLoc, secondG)} {spinnerWeightUnits.Text}";
                    cExpanded.Add(tmpDataLight);
                }
            }

            var adapter = new CompareAllAdapter(currentObjects);

            listView.Adapter = adapter;

            Dialog.SetView(dialogView);

            Dialog.SetOnCancelListener(new OnCancelListener(dialogView));

            Dialog.SetPositiveButton(Resource.String.lblMoreInfo, MoreInfo_Click);
            Dialog.SetNegativeButton(Resource.String.btnCancel, (sender, e) => { });
            Dialog.SetNeutralButton(Resource.String.lblShare, ShareComparison_Click);

            Dialog.Show();
        }

        public string ToCSV<T>(List<T> lstData)
        {
            var info = typeof(T).GetProperties();
            var sb = new StringBuilder();
            foreach (var obj in lstData)
            {
                var line = "";
                foreach (var prop in info)
                {
                    line += $"{prop.GetValue(obj, null)}, ";
                }
                sb.AppendLine(line[0..^2]);
            }
            return sb.ToString();
        }

        private void ShareComparison_Click(object sender, DialogClickEventArgs e)
        {
            Intent share = new(Intent.ActionSend);
            share.SetType(MAIL_TYPE);
            share.AddFlags(ActivityFlags.NewDocument);

            try
            {
                File file = CreateDirFile($"gravity_comparison_{DateTime.Now:dd_MM_yyyy_hh_mm_ss}.csv");

                try
                {
                    FileOutputStream fout = new(file);
                    if (string.IsNullOrEmpty(txtWeight.Text))
                    {
                        cLight.Insert(0, new CelestialDataLight(GetString(Resource.String.LblName), GetString(Resource.String.LblCO), GetString(Resource.String.LblGravity), GetString(Resource.String.LblComparison)));
                    fout.Write(Encoding.UTF8.GetBytes(ToCSV(cLight)));
                    }
                    else
                    {
                        cExpanded.Insert(0, new CelestialDataExpanded(GetString(Resource.String.LblName), GetString(Resource.String.LblCO), GetString(Resource.String.LblGravity), GetString(Resource.String.LblComparison), GetString(Resource.String.lblShareMsgTitle)));
                        fout.Write(Encoding.UTF8.GetBytes(ToCSV(cExpanded)));
                    }
                    fout.Close();
                }
                catch
                {
                    Toast.MakeText(Context, GetString(Resource.String.LblStorageIssue), ToastLength.Long).Show();
                }

                if (file.Exists())
                {
                    share.PutExtra(Intent.ExtraSubject, Context.GetString(Resource.String.lblSubjectEmail));
                    share.PutExtra(Intent.ExtraText, Context.GetString(Resource.String.titleThanksToGNow));

                    if (Build.VERSION.SdkInt < BuildVersionCodes.N)
                    {
                        share.PutExtra(Intent.ExtraStream, Android.Net.Uri.FromFile(file.AbsoluteFile));
                    }
                    else
                    {
                        share.AddFlags(ActivityFlags.GrantReadUriPermission);
                        share.AddFlags(ActivityFlags.GrantWriteUriPermission);
                        var contentUri = AndroidX.Core.Content.FileProvider.GetUriForFile(Application.Context, FILE_PROVIDER, file);
                        share.PutExtra(Intent.ExtraStream, contentUri);
                    }
                    Context.StartActivity(Intent.CreateChooser(share, Context.GetString(Resource.String.txtEmail)));
                }
            }
            catch { }
        }

        private void MoreInfo_Click(object sender, DialogClickEventArgs e)
        {
            if (!OpenURL(NASA_LINK))
            {
                Toast.MakeText(Context, Context.GetString(Resource.String.LblOnline), ToastLength.Long).Show();
            }
        }

        private void SetBasics()
        {
            CalculateGravity();

            SetGravity();

            CalculateWeight();
        }

        private async void GetProfile()
        {
            try
            {
                profile = await new ProfileDAO().GetProfile();

                spinnerGravityUnits.SetText(gUnits[(int)profile.SelectedUnits], false);

                spinnerWeightUnits.SetText(weightUnits[(int)profile.SelectedUnits], false);

                SetBasics();
            }
            catch
            {
                try
                {
                    SetBasics();
                }
                catch { }
            }
        }

        private void TxtWeight_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            CalculateWeight();
        }

        private int GetItemPosition(List<string> options, string chosen)
        {
            return Array.IndexOf(options.ToArray(), chosen);
        }

        private void CalculateWeight()
        {
            bool isNumeric = double.TryParse(txtWeight.Text, out double earthWeight);
            if (isNumeric && earthWeight > 0)
            {
                GravityPlanets data = new();

                double weight = earthWeight;

                int firstCelestialLoc = GetItemPosition(planets, spinnerFirstCelestial.Text);
                int secondCelestialLoc = GetItemPosition(planets, spinnerSecondCelestial.Text);

                double newWeight = GetNewWeight(data, weight, firstCelestialLoc, secondCelestialLoc);
                SetWeight(weight, newWeight);
            }
            else
            {
                lblDWeight.Visibility = ViewStates.Gone;
                imgWeight.Visibility = ViewStates.Gone;
                btnShare.Visibility = ViewStates.Gone;
            }
        }

        private static double GetNewWeight(GravityPlanets data, double weight, int firstCelestialLoc, int secondCelestialLoc)
        {
            double gravity1 = data.GetGravity(firstCelestialLoc);
            double gravity2 = data.GetGravity(secondCelestialLoc);

            double difference = gravity2 / gravity1;

            double newWeight;
            if (firstCelestialLoc != 3)
            {
                var temp = data.GetGravity(firstCelestialLoc) / data.GetGravity(3);
                newWeight = weight * temp;

                newWeight *= difference;
            }
            else
            {
                newWeight = weight * difference;
            }

            return Math.Round(newWeight, 1);
        }

        private void SetWeight(double initialWeight, double newWeight)
        {
            imgWeight.Visibility = ViewStates.Gone;
            lblDWeight.Visibility = ViewStates.Visible;
            btnShare.Visibility = ViewStates.Gone;

            int secondCelestialLoc = GetItemPosition(planets, spinnerSecondCelestial.Text);

            string extra = "";

            if (new int[] { 0, 3, 4 }.Contains(secondCelestialLoc))
            {
                extra = $"{GetString(Resource.String.thePlus)} ";
            }

            if (newWeight < initialWeight)
            {
                lblDWeight.SetTextColor(Color.ParseColor(GREEN_COLOR));

                secondWeight = $"{newWeight}{spinnerWeightUnits.Text}";

                lblDWeight.Text = string.Format(GetString(Resource.String.lightWeight), $"{extra}{GetPlanet(secondCelestialLoc)}", newWeight, spinnerWeightUnits.Text);
                SetImage(Resource.Drawable.light, imgWeight);
                imgWeight.Visibility = ViewStates.Visible;
                btnShare.Visibility = ViewStates.Visible;
            }
            else if (newWeight > initialWeight)
            {
                lblDWeight.SetTextColor(Color.ParseColor(RED_COLOR));

                secondWeight = $"{newWeight}{spinnerWeightUnits.Text}";

                lblDWeight.Text = string.Format(GetString(Resource.String.heavyWeight), $"{extra}{GetPlanet(secondCelestialLoc)}", newWeight, spinnerWeightUnits.Text);
                SetImage(Resource.Drawable.weight, imgWeight);
                imgWeight.Visibility = ViewStates.Visible;
                btnShare.Visibility = ViewStates.Visible;
            }
            else
            {
                lblDWeight.SetTextColor(Color.ParseColor(BLUE_COLOR));
                lblDWeight.Text = GetString(Resource.String.sameWeight);
            }
        }

        private void CalculateGravity()
        {
            GravityPlanets gInfo = new();

            int firstCelestialLoc = GetItemPosition(planets, spinnerFirstCelestial.Text);
            int secondCelestialLoc = GetItemPosition(planets, spinnerSecondCelestial.Text);

            var gPercentage = gInfo.PercentageGravity(firstCelestialLoc, secondCelestialLoc);

            string extra = "";

            if (new int[] {0, 3, 4 }.Contains(firstCelestialLoc))
            {
                extra = $"{GetString(Resource.String.thePlus)} ";
            }

            switch (gInfo.ComparedGravity(firstCelestialLoc, secondCelestialLoc))
            {
                case 0:
                    lblGDescription.SetTextColor(Color.ParseColor(GREEN_COLOR));
                    lblGDescription.Text = $"{string.Format(GetString(Resource.String.gravBigger), $"{extra}{GetPlanet(firstCelestialLoc)}")} {Math.Round(gPercentage, 0)}%";
                    btnShare.Visibility = ViewStates.Visible;
                    break;
                case 1:
                    lblGDescription.SetTextColor(Color.ParseColor(RED_COLOR));
                    lblGDescription.Text = $"{string.Format(GetString(Resource.String.gravSmaller), $"{extra}{GetPlanet(firstCelestialLoc)}")} {Math.Round(gPercentage, 0)}%";
                    btnShare.Visibility = ViewStates.Visible;
                    break;
                case 2:
                    lblGDescription.SetTextColor(Color.ParseColor(BLUE_COLOR));
                    lblGDescription.Text = GetString(Resource.String.gravSame);
                    break;
            }
        }

        private void SetPlanet(int planet, ImageView imgPlanet)
        {
            imgPlanet.SetImageDrawable(compareAllList[planet].image);
            SetGravity();
        }

        public void SetGravity()
        {
            GravityPlanets gInfo = new();

            int firstCelestialLoc = GetItemPosition(planets, spinnerFirstCelestial.Text);
            int secondCelestialLoc = GetItemPosition(planets, spinnerSecondCelestial.Text);
            int intGUnitsLoc = GetItemPosition(gUnits, spinnerGravityUnits.Text);

            if (intGUnitsLoc == 0)
            {
                lblGFirstCelestial.Text = gInfo.GetGravity(firstCelestialLoc).ToString();
                lblGSecondCelestial.Text = gInfo.GetGravity(secondCelestialLoc).ToString();
            }
            else
            {
                var gravity = new Supernova.Core.Gravity();
                lblGFirstCelestial.Text = Math.Round(gravity.ChangeToFeet(gInfo.GetGravity(firstCelestialLoc)), 4).ToString();

                lblGSecondCelestial.Text = Math.Round(gravity.ChangeToFeet(gInfo.GetGravity(secondCelestialLoc)), 4).ToString();
            }

            SetCelestialObjectType(gInfo.CelestialObjectType(firstCelestialLoc), lblDFirstCelestial);
            SetCelestialObjectType(gInfo.CelestialObjectType(secondCelestialLoc), lblDSecondCelestial);
        }

        private void SetCelestialObjectType(CelestialObjectTypes cType, TextView txtCObject)
        {
            txtCObject.Text = GetCelestialObject(cType);
        }

        private string GetCelestialObject(CelestialObjectTypes cType)
        {
            return cType switch
            {
                CelestialObjectTypes.DwarfPlanet => GetString(Resource.String.itemDwarfPlanet),
                CelestialObjectTypes.Moon => GetString(Resource.String.itemMoon),
                CelestialObjectTypes.Planet => GetString(Resource.String.itemPlanet),
                _ => GetString(Resource.String.itemStar),
            };
        }

        public string GetPlanet(int planet)
        {
            return planets[planet];
        }

        public Drawable GetCelestialObjectDrawable(int planetID)
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                return Resources.GetDrawable(planetID, Activity.Theme);
            }
            else
            {
#pragma warning disable CS0618 // Type or member is obsolete
                return Resources.GetDrawable(planetID);
#pragma warning restore CS0618 // Type or member is obsolete
            }
        }

        private void SetImage(int planetID, ImageView imgPlanet)
        {
            imgPlanet.SetImageDrawable(GetCelestialObjectDrawable(planetID));
        }
    }
}
