﻿using System.Collections.Generic;
using AndroidX.Fragment.App;
using AndroidX.ViewPager2.Adapter;

namespace GNow_Android.Adapters
{
	class HelpAdapter : FragmentStateAdapter
	{
		private readonly List<Fragment> fragments;

		public HelpAdapter(FragmentActivity fragmentActivity) : base(fragmentActivity)
		{
			fragments = new List<Fragment>()
			{
				DefinitionFragment.NewInstance(),
				GravityCFragment.NewInstance(),
				FormulaFragment.NewInstance()
			};
		}

		public override int ItemCount
		{
			get { return fragments.Count; }
		}

		public override Fragment CreateFragment(int p0)
        {
			return fragments[p0];
		}
	}
}
