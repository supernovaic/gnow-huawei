﻿using AndroidX.ViewPager2.Widget;

namespace GNow_Android
{
    public partial class MainActivity
    {
        private class OnPageChangeCallBack : ViewPager2.OnPageChangeCallback
        {
            private readonly ViewPager2 viewPager2;

            public OnPageChangeCallBack(ViewPager2 viewPager2)
            {
                this.viewPager2 = viewPager2;
            }

            public override void OnPageSelected(int position)
            {
                base.OnPageSelected(position);

                viewPager2.UserInputEnabled = position != 4;
            }
        }
    }
}