﻿using System.Collections.Generic;

namespace Supernova.Model.Core
{
    public enum CelestialObject
    {
        Sun,
        Mercury,
        Venus,
        Earth,
        Moon,
        Mars,
        Phobos,
        Deimos,
        Ceres,

        Jupiter,
        Io,
        Europa,
        Amalthea,
        Ganymede,
        Callisto,

        Saturn,
        Daphnis,
        Mimas,
        Enceladus,
        Tethys,
        Dione,
        Rhea,
        Titan,
        Helene,
        Pan,
        Telesto,
        Hyperion,
        Iapetus,
        Phoebe,
        Janus,
        Pandora,
        Atlas,

        Uranus,
        Ariel,
        Umbriel,
        Titania,
        Oberon,
        Miranda,

        Neptune,
        Triton,
        Proteus,

        Pluto,
        Charon,
        Nix,

        Eris,
        Makemake,
        Haumea
    }

    public enum CelestialObjectTypes
    {
        Star,
        Planet,
        DwarfPlanet,
        Moon
    }

    class Planets
    {
        public int idPlanet;
        public CelestialObject idCelestialObject;
        public CelestialObjectTypes planetType;
        public double gravity;
    }

    public class GravityPlanets
    {
        internal List<Planets> Place { get; set; }

        public GravityPlanets()
        {
            Place = new List<Planets>
            {
                new Planets() { idPlanet = 0, idCelestialObject = CelestialObject.Sun, planetType = CelestialObjectTypes.Star, gravity = 274 },
                new Planets() { idPlanet = 1, idCelestialObject = CelestialObject.Mercury, planetType = CelestialObjectTypes.Planet, gravity = 3.7 },
                new Planets() { idPlanet = 2, idCelestialObject = CelestialObject.Venus, planetType = CelestialObjectTypes.Planet, gravity = 8.87 },
                new Planets() { idPlanet = 4, idCelestialObject = CelestialObject.Earth, planetType = CelestialObjectTypes.Planet, gravity = 9.798 },
                new Planets() { idPlanet = 5, idCelestialObject = CelestialObject.Moon, planetType = CelestialObjectTypes.Moon, gravity = 1.62 },
                new Planets() { idPlanet = 6, idCelestialObject = CelestialObject.Mars, planetType = CelestialObjectTypes.Planet, gravity = 3.71 },
                new Planets() { idPlanet = 7, idCelestialObject = CelestialObject.Phobos, planetType = CelestialObjectTypes.Moon, gravity = 0.0057 },
                new Planets() { idPlanet = 8, idCelestialObject = CelestialObject.Deimos, planetType = CelestialObjectTypes.Moon, gravity = 0.003 },
                new Planets() { idPlanet = 9, idCelestialObject = CelestialObject.Ceres, planetType = CelestialObjectTypes.DwarfPlanet, gravity = 0.27 },
                new Planets() { idPlanet = 10, idCelestialObject = CelestialObject.Jupiter, planetType = CelestialObjectTypes.Planet, gravity = 24.92 },
                new Planets() { idPlanet = 11, idCelestialObject = CelestialObject.Io, planetType = CelestialObjectTypes.Moon, gravity = 1.796 },
                new Planets() { idPlanet = 12, idCelestialObject = CelestialObject.Europa, planetType = CelestialObjectTypes.Moon, gravity = 1.315 },
                new Planets() { idPlanet = 13, idCelestialObject = CelestialObject.Amalthea, planetType = CelestialObjectTypes.Moon, gravity = 0.02 },
                new Planets() { idPlanet = 14, idCelestialObject = CelestialObject.Ganymede, planetType = CelestialObjectTypes.Moon, gravity = 1.428 },
                new Planets() { idPlanet = 15, idCelestialObject = CelestialObject.Callisto, planetType = CelestialObjectTypes.Moon, gravity = 1.236 },

                new Planets() { idPlanet = 16, idCelestialObject = CelestialObject.Saturn, planetType = CelestialObjectTypes.Planet, gravity = 10.44 },

                new Planets() { idPlanet = 17, idCelestialObject = CelestialObject.Daphnis, planetType = CelestialObjectTypes.Moon, gravity = 0.00036 },
                new Planets() { idPlanet = 18, idCelestialObject = CelestialObject.Mimas, planetType = CelestialObjectTypes.Moon, gravity = 0.064 },
                new Planets() { idPlanet = 19, idCelestialObject = CelestialObject.Enceladus, planetType = CelestialObjectTypes.Moon, gravity = 0.113 },
                new Planets() { idPlanet = 20, idCelestialObject = CelestialObject.Tethys, planetType = CelestialObjectTypes.Moon, gravity = 0.145 },
                new Planets() { idPlanet = 21, idCelestialObject = CelestialObject.Dione, planetType = CelestialObjectTypes.Moon, gravity = 0.232 },
                new Planets() { idPlanet = 22, idCelestialObject = CelestialObject.Rhea, planetType = CelestialObjectTypes.Moon, gravity = 0.264 },
                new Planets() { idPlanet = 23, idCelestialObject = CelestialObject.Titan, planetType = CelestialObjectTypes.Moon, gravity = 1.352 },
                new Planets() { idPlanet = 24, idCelestialObject = CelestialObject.Helene, planetType = CelestialObjectTypes.Moon, gravity = 0.002 },
                new Planets() { idPlanet = 25, idCelestialObject = CelestialObject.Pan, planetType = CelestialObjectTypes.Moon, gravity = 0.001 },
                new Planets() { idPlanet = 26, idCelestialObject = CelestialObject.Telesto, planetType = CelestialObjectTypes.Moon, gravity = 0.002 },
                new Planets() { idPlanet = 27, idCelestialObject = CelestialObject.Hyperion, planetType = CelestialObjectTypes.Moon, gravity = 0.02 },
                new Planets() { idPlanet = 28, idCelestialObject = CelestialObject.Iapetus, planetType = CelestialObjectTypes.Moon, gravity = 0.223 },
                new Planets() { idPlanet = 29, idCelestialObject = CelestialObject.Phoebe, planetType = CelestialObjectTypes.Moon, gravity = 0.049 },
                new Planets() { idPlanet = 30, idCelestialObject = CelestialObject.Janus, planetType = CelestialObjectTypes.Moon, gravity = 0.016 },
                new Planets() { idPlanet = 31, idCelestialObject = CelestialObject.Pandora, planetType = CelestialObjectTypes.Moon, gravity = 0.0034 },
                new Planets() { idPlanet = 31, idCelestialObject = CelestialObject.Atlas, planetType = CelestialObjectTypes.Moon, gravity = 0.002 },

                new Planets() { idPlanet = 32, idCelestialObject = CelestialObject.Uranus, planetType = CelestialObjectTypes.Planet, gravity = 8.69 },

                new Planets() { idPlanet = 33, idCelestialObject = CelestialObject.Ariel, planetType = CelestialObjectTypes.Moon, gravity = 0.269 },
                new Planets() { idPlanet = 34, idCelestialObject = CelestialObject.Umbriel, planetType = CelestialObjectTypes.Moon, gravity = 0.2 },
                new Planets() { idPlanet = 35, idCelestialObject = CelestialObject.Titania, planetType = CelestialObjectTypes.Moon, gravity = 0.367 },
                new Planets() { idPlanet = 36, idCelestialObject = CelestialObject.Oberon, planetType = CelestialObjectTypes.Moon, gravity = 0.346 },
                new Planets() { idPlanet = 37, idCelestialObject = CelestialObject.Miranda, planetType = CelestialObjectTypes.Moon, gravity = 0.079 },

                new Planets() { idPlanet = 38, idCelestialObject = CelestialObject.Neptune, planetType = CelestialObjectTypes.Planet, gravity = 11.15 },
                new Planets() { idPlanet = 39, idCelestialObject = CelestialObject.Triton, planetType = CelestialObjectTypes.Moon, gravity = 0.779 },
                new Planets() { idPlanet = 40, idCelestialObject = CelestialObject.Proteus, planetType = CelestialObjectTypes.Moon, gravity = 0.07 },

                new Planets() { idPlanet = 41, idCelestialObject = CelestialObject.Pluto, planetType = CelestialObjectTypes.DwarfPlanet, gravity = 0.58 },

                new Planets() { idPlanet = 42, idCelestialObject = CelestialObject.Charon, planetType = CelestialObjectTypes.Moon, gravity = 0.288 },
                new Planets() { idPlanet = 43, idCelestialObject = CelestialObject.Nix, planetType = CelestialObjectTypes.Moon, gravity = 0.000163 },

                new Planets() { idPlanet = 44, idCelestialObject = CelestialObject.Eris, planetType = CelestialObjectTypes.DwarfPlanet, gravity = 0.82 },
                new Planets() { idPlanet = 45, idCelestialObject = CelestialObject.Makemake, planetType = CelestialObjectTypes.DwarfPlanet, gravity = 0.5 },
                new Planets() { idPlanet = 46, idCelestialObject = CelestialObject.Haumea, planetType = CelestialObjectTypes.DwarfPlanet, gravity = 0.401 }
            };
        }

        public int ComparedGravity(int id1, int id2)
        {
            double gravity1 = Place[id1].gravity;
            double gravity2 = Place[id2].gravity;

            return gravity1 > gravity2 ? 0 : gravity1 < gravity2 ? 1 : 2;
        }

        public double PercentageGravity(int id1, int id2)
        {
            int result = ComparedGravity(id1, id2);

            double gravity1 = Place[id1].gravity;
            double gravity2 = Place[id2].gravity;

            return result switch
            {
                0 => gravity1 * 100 / gravity2,
                1 => gravity2 * 100 / gravity1,
                _ => 1
            };
        }

        public double GetGravity(int id)
        {
            return Place[id].gravity;
        }

        public CelestialObjectTypes CelestialObjectType(int id)
        {
            return Place[id].planetType;
        }
    }
}