﻿using SQLite;
using Supernova.Database.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Supernova.Database.DAO
{
    class ProfileDAO : DatabaseManagement
    {
        static SQLiteAsyncConnection con;
        public ProfileDAO()
        {
            con = ConnectionDb();
        }
        public static void Delete(int id)
        {
            
        }
        public async static void Insert(Profile obj)
        {
            new ProfileDAO();
            await con.InsertAsync(obj);
        }

        public async static void Update(Profile obj)
        {
            new ProfileDAO();
            await con.UpdateAsync(obj);
        }

        public async Task<Profile> GetProfile()
        {
            return (await con.QueryAsync<Profile>("select * from Profile")).FirstOrDefault();
        }
    }
}
