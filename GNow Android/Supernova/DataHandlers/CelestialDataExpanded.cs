﻿using System;

namespace Supernova.Model.Generic
{
    public class CelestialDataExpanded
    {
        public CelestialDataExpanded()
        {

        }

        public CelestialDataExpanded(string name, string type, string gravity, string comparison, string myweight)
        {
            Name = name;
            Type = type;
            Gravity = gravity;
            Comparison = comparison;
            MyWeight = myweight;
        }

        public string Name { get; set; }
        public string Type { get; set; }
        public string Gravity { get; set; }
        public string Comparison { get; set; }
        public string MyWeight { get; set; }
    }
}

