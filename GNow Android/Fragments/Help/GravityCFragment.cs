﻿using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using Bumptech.Glide;
using Bumptech.Glide.Request.Target;

namespace GNow_Android
{
    public class GravityCFragment : Fragment
    {

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.HelpGravityC, container, false);

            var gifPreview = view.FindViewById<ImageView>(Resource.Id.gifPreview);

            Glide.With(Context).Load(Resource.Drawable.globe_animation).Into(new DrawableImageViewTarget(gifPreview));

            return view;
        }

        public static Fragment NewInstance()
        {
            return new GravityCFragment();
        }
   }
}
