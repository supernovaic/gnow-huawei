﻿using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using Bumptech.Glide;
using Bumptech.Glide.Request.Target;
using Google.Android.Material.TextField;
using Huawei.Hms.Maps;
using Huawei.Hms.Maps.Model;
using Newtonsoft.Json;
using Supernova.Database.DAO;
using Supernova.Database.Entities;
using Supernova.Enums;
using Supernova.Model.Generic;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using static Android.Widget.AdapterView;

namespace GNow_Android
{
    public class OSMSearchFragment : Fragment, IOnMapReadyCallback
    {
        private const string OSM_ADDRESS = "https://nominatim.openstreetmap.org/search/{0}?format=json&email=fanmixco@gmail.com";
        private const string GEO_NAMES = "http://api.geonames.org/srtm3JSON?lat={0}&lng={1}&username=fanmixco";

        private ImageView gifImageProgressView;
        private ListView lstOSMResults;
        private TextInputEditText txtSearch;
        private GravityOSM selectedValue;
        private LinearLayout imgContainer;
        private ImageView imgMapSearch;

        private Profile profile;

        public static Fragment NewInstance()
        {
            return new OSMSearchFragment();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.OSMSearch, container, false);

            lstOSMResults = view.FindViewById<ListView>(Resource.Id.lstOSMResults);
            lstOSMResults.ItemClick += LstOSMResults_ItemClick;
            txtSearch = view.FindViewById<TextInputEditText>(Resource.Id.txtSearch);
            imgContainer = view.FindViewById<LinearLayout>(Resource.Id.imgContainer);
            txtSearch.TextChanged += TxtSearch_TextChanged;

            gifImageProgressView = view.FindViewById<ImageView>(Resource.Id.gifImageProgressView);

            Glide.With(Context).Load(Resource.Drawable.loading_icon).Into(new DrawableImageViewTarget(gifImageProgressView));

            imgMapSearch = view.FindViewById<ImageView>(Resource.Id.imgMapSearch);

            GetProfile();

            return view;
        }

        private async void GetProfile()
        {
            try
            {
                profile = await new ProfileDAO().GetProfile();
            }
            catch { }
        }

        private void LstOSMResults_ItemClick(object sender, ItemClickEventArgs e)
        {
            selectedValue = lstOSMResults.GetItemAtPosition(e.Position).Cast<GravityOSM>();

            var dialog = new Android.App.Dialog(Activity, Resource.Style.Theme_GNowMapAlertTheme);
            dialog.SetTitle(selectedValue.address);
            dialog.SetContentView(Resource.Layout.MapPreview);
            dialog.Show();
            _ = (MapView)dialog.FindViewById(Resource.Id.mapView);
            MapsInitializer.Initialize(Activity);

            MapView mMapView = (MapView)dialog.FindViewById(Resource.Id.mapView);
            mMapView.OnCreate(dialog.OnSaveInstanceState());
            mMapView.OnResume();// needed to get the map to display immediately
            mMapView.GetMapAsync(this);
        }

        private async Task<int> GetAltitude(double latitude, double longitude)
        {
            try
            {
                using var client = new HttpClient();
                var result = await client.GetAsync(new Uri(string.Format(GEO_NAMES, latitude, longitude)));
                if (result.IsSuccessStatusCode && result.StatusCode == HttpStatusCode.OK)
                {
                    //ok to process
                    var json = await result.Content.ReadAsStringAsync();
                    var status = JsonConvert.DeserializeObject<ResultData>(json);
                    return status.srtm3; ;
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }

        private async void TxtSearch_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            if (txtSearch.Text.Length == 0)
            {
                UpdateListView(new List<GravityOSM>());
                return;
            }
            imgMapSearch.Visibility = ViewStates.Gone;
            gifImageProgressView.Visibility = ViewStates.Visible;

            var gOSM = new List<GravityOSM>();

            try
            {
                HttpClientHandler handler = new();
                handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
                using var client = new HttpClient(handler);
                var result = await client.GetAsync(new Uri(string.Format(OSM_ADDRESS, txtSearch.Text)));
                if (result.IsSuccessStatusCode && result.StatusCode == HttpStatusCode.OK)
                {
                    //ok to process
                    var json = await result.Content.ReadAsStringAsync();
                    var status = JsonConvert.DeserializeObject<List<ResultLocation>>(json);
                    Supernova.Core.Gravity G;
                    if (profile.SelectedUnits == Units.Meters)
                    {
                        foreach (var location in status)
                        {
                            try
                            {
                                var latitude = double.Parse(location.lat);
                                var longitude = double.Parse(location.lon);
                                var altitude = await GetAltitude(latitude, longitude);

                                G = new Supernova.Core.Gravity(latitude, longitude, altitude);

                                double gravity = GetGravity(G);

                                gOSM.Add(new GravityOSM()
                                {
                                    latitude = latitude,
                                    longitude = longitude,
                                    altitude = altitude,
                                    addressKeyword = $"{GetString(Resource.String.lblLocation)}: ",
                                    address = location.display_name,
                                    gravity = $"{Math.Round(gravity, 5)} m/s²",
                                    gravityKeyword = $"{GetString(Resource.String.lblGravity2)} "
                                });
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        foreach (var location in status)
                        {
                            try
                            {
                                var latitude = double.Parse(location.lat);
                                var longitude = double.Parse(location.lon);
                                var altitude = await GetAltitude(latitude, longitude);

                                G = new Supernova.Core.Gravity(latitude, longitude, altitude);

                                double gravity = GetGravity(G);

                                gOSM.Add(new GravityOSM()
                                {
                                    latitude = latitude,
                                    longitude = longitude,
                                    altitude = altitude,
                                    addressKeyword = $"{GetString(Resource.String.lblLocation)}: ",
                                    address = location.display_name,
                                    gravity = $"{Math.Round(G.ChangeToFeet(gravity), 5)} ft/s²",
                                    gravityKeyword = $"{GetString(Resource.String.lblGravity2)} "
                                });
                            }
                            catch { }
                        }
                    }
                }
            }
            catch { }

            UpdateListView(gOSM);
        }

        private static double GetGravity(Supernova.Core.Gravity G)
        {
            var gravity = G.GetGravity();

            if (gravity < 9.7639)
            {
                gravity = 9.798;
            }

            return gravity;
        }

        private void UpdateListView(List<GravityOSM> data)
        {
            if (data.Count > 0)
            {
                imgContainer.Visibility = ViewStates.Gone;
                lstOSMResults.Visibility = ViewStates.Visible;
                lstOSMResults.Adapter = new GravityOSMAdapter(data);
            }
            else
            {
                imgContainer.Visibility = ViewStates.Visible;
                lstOSMResults.Visibility = ViewStates.Gone;
            }
            gifImageProgressView.Visibility = ViewStates.Gone;
            imgMapSearch.Visibility = ViewStates.Visible;
        }

        public void OnMapReady(HuaweiMap googleMap)
        {
            MarkerOptions markerOpt1 = new();

            googleMap.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(new LatLng(selectedValue.latitude, selectedValue.longitude), 10));

            googleMap.UiSettings.ZoomControlsEnabled = true;

            markerOpt1.InvokePosition(new LatLng(selectedValue.latitude, selectedValue.longitude));

            markerOpt1.InvokeTitle(selectedValue.gravity);

            googleMap.AddMarker(markerOpt1).ShowInfoWindow();
        }
    }
}
