using Android.OS;
using Android.Views;
using AndroidX.AppCompat.App;
using AndroidX.ViewPager2.Widget;
using Com.Github.Florent37.Bubbletab;
using GNow_Android.Adapters;
using Huawei.Hms.Ads;
using Huawei.Hms.Ads.Banner;
using Google.Android.Material.NavigationRail;

namespace GNow_Android
{
    [Android.App.Activity(Label = "Help")]
    public partial class Help : AppCompatActivity
    {
        public ActionBar HelpActionBar { get; set; }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetTheme(Resource.Style.Theme_GNowTheme_Material);

            Title = GetString(Resource.String.lblHelp);

            SetContentView(Resource.Layout.HelpLayout);

            HwAds.Init(this);

            FindViewById<BannerView>(Resource.Id.adView).LoadAd(new AdParam.Builder().Build());
/*#if DEBUG
            FindViewById<BannerView>(Resource.Id.adView).Visibility = ViewStates.Gone;
#endif*/

            HelpActionBar = SupportActionBar;

            HelpActionBar.SetHomeAsUpIndicator(Resource.Drawable.arrow_left);

            HelpActionBar.SetDisplayHomeAsUpEnabled(true);

            if (Resources.Configuration.Orientation == Android.Content.Res.Orientation.Portrait)
            {
                var bubbleTab = FindViewById<BubbleTab>(Resource.Id.bubbleTabHelp);
                var viewPager = FindViewById<ViewPager2>(Resource.Id.viewPagerHelp);

            viewPager.Adapter = new HelpAdapter(this);

            bubbleTab.SetupWithViewPager(viewPager);
            }
            else
            {
                var sideTab = FindViewById<NavigationRailView>(Resource.Id.help_navigation);

                sideTab.ItemSelected += SideTab_ItemSelected;

                sideTab.SelectedItemId = Resource.Id.menu_home;
            }
        }

        private void SideTab_ItemSelected(object sender, Google.Android.Material.Navigation.NavigationBarView.ItemSelectedEventArgs e)
        {
            LoadFragment(e.P0.ItemId);
        }

        private void LoadFragment(int id)
        {
            AndroidX.Fragment.App.Fragment fragment = null;
            switch (id)
            {
                case Resource.Id.menu_home:
                    fragment = DefinitionFragment.NewInstance();
                    break;
                case Resource.Id.menu_open:
                    fragment = GravityCFragment.NewInstance();
                    break;
                case Resource.Id.menu_math:
                    fragment = FormulaFragment.NewInstance();
                    break;
            }

            if (fragment == null)
            {
                return;
            }

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame_data_help, fragment)
                .Commit();
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutInt("tab", HelpActionBar.SelectedNavigationIndex);

            base.OnSaveInstanceState(outState);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    base.OnBackPressed();
                    return true;
                default:
                    return false;
            }
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            Finish();
        }
    }
}
