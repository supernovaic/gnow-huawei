﻿using Android.OS;
using Android.Views;
using AndroidX.Fragment.App;

namespace GNow_Android.Fragments
{
    public class DrawerFragment : Fragment
    {
        public DrawerFragment() { }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Inflate the layout for this fragment
            return inflater.Inflate(Resource.Layout.DrawerFragment, container, false);
        }
    }
}