﻿using Android.Content;
using Android.Views;

namespace GNow_Android
{
    public partial class ComparisonFragment
    {
        private class OnDismissListener : Java.Lang.Object, IDialogInterfaceOnDismissListener
        {
            public void OnDismiss(IDialogInterface dialog)
            {
                dialog.Dispose();
            }
        }

        private class OnCancelListener : Java.Lang.Object, IDialogInterfaceOnCancelListener
        {
            private View dialogView;

            public OnCancelListener(View dialogView)
            {
                this.dialogView = dialogView;
            }

            public void OnCancel(IDialogInterface dialog)
            {
                dialogView.Dispose();
                dialogView = null;
                dialog.Dispose();
                dialog = null;
            }
        }
    }
}
