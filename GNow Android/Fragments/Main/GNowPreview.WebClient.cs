﻿using Android.Net;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace GNow_Android
{
    public partial class GNowPreview
    {
        private class WebClient : WebViewClient
        {
            private readonly ProgressBar indeterminateBar;
            private readonly WebView webView;

            public WebClient(ProgressBar indeterminateBar, WebView webView)
            {
                this.indeterminateBar = indeterminateBar;
                this.webView = webView;
            }

            public override void OnPageFinished(WebView view, string url)
            {
                indeterminateBar.Visibility = ViewStates.Gone;
                webView.Visibility = ViewStates.Visible;
            }

            [System.Obsolete]
            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {
                return !Uri.Parse(url).Host.Contains("fanmixco.github.io");
            }

            public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
            {
                return !request.Url.ToString().Contains("fanmixco.github.io");
            }
        }
    }
}