﻿using System;

namespace Supernova.Core
{
    class Gravity
    {
        /*Basic Constants
         * Max height
         * Min height
         * Value of feets
         */
        private const int EVEREST = 6168;
        private const int DEAD_SEA = -427;
        private const double FOOT = 3.2808399;

        public double Latitude { get; set; }
        public double Altitude { get; set; }
        public double Longitude { get; set; }

        public Gravity()
        {
            Longitude = 0;
            Latitude = 0;
            Altitude = 0;
        }

        public Gravity(double lat, double lng, double alt)
        {
            Longitude = lng;
            Latitude = lat;
            Altitude = alt;
        }

        /*
         * 0 means invalid latitude
         * 1 means higher than Everest
         * 2 means deeper than Dead Sea
         * Else other values means a gravity
         */
        public double GetGravity()
        {
            #region validations
            if (Latitude < -90 || Latitude > 90)
            {
                return 0;
            }

            if (Altitude > EVEREST)
            {
                return 1;
            }

            if (Altitude < DEAD_SEA)
            {
                return 2;
            }
            #endregion

            var IGF = 9.780327 * (1 + 0.0053024 * Math.Pow(Math.Sin(Latitude), 2) - 0.0000058 * Math.Pow(Math.Sin(2 * Latitude), 2));

            var FAC = -3.086 * Math.Pow(10, -6) * Altitude;

            var g = IGF + FAC;

            //correction if there is any invalid gravity, it will return the average one, it can happen in the sea side for example, the minimum gravity is from Mount Nevado Huascarán in Peru
            if (g < 9.7639)
            {
                g = 9.798;
            }

            return g;
        }

        public double ChangeToMetres(double value)
        {
            return value / FOOT;
        }

        public double ChangeToFeet(double value)
        {
            return value * FOOT;
        }
    }
}
