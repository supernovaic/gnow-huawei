﻿using System.Collections.Generic;
using AndroidX.Fragment.App;
using AndroidX.ViewPager2.Adapter;

namespace GNow_Android.Adapters
{
    class MainAdapter : FragmentStateAdapter
	{
        private readonly List<Fragment> fragments;
		public MainAdapter(FragmentActivity fragmentActivity) : base(fragmentActivity)
        {

            fragments = new List<Fragment>()
            {
                HomeFragment.NewInstance(),
                CalcFragment.NewInstance(),
                ComparisonFragment.NewInstance(),
                OSMSearchFragment.NewInstance(),
                GNowPreview.NewInstance()
            };
        }

        public override int ItemCount
        {
            get { return fragments.Count; }
        }

        public override Fragment CreateFragment(int p0)
        {
            return fragments[p0];
        }
	}
}
