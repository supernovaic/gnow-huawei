﻿using Android.OS;
using Android.Views;
using AndroidX.Fragment.App;

namespace GNow_Android
{
    public class FormulaFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            return inflater.Inflate(Resource.Layout.HelpFormula, container, false);
        }
        public static Fragment NewInstance()
        {
            return new FormulaFragment();
        }
    }
}