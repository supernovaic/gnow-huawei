﻿using SQLite;
using Supernova.Enums;
using System;

namespace Supernova.Database.Entities
{
    public class Profile
    {
        [PrimaryKey, AutoIncrement]
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool Sync { get; set; }
        public Units SelectedUnits { get; set; }
        public int NDecimal { get; set; }
        public int CalcDecimal { get; set; }
    }

    class GravityData
    {
        [PrimaryKey, AutoIncrement]

        public int IdGravity { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Altitude { get; set; }
        public double Gravity { get; set; }
        public DateTime Registered { get; set; }
    }
}