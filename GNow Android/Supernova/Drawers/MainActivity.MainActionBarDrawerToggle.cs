﻿using Android.Views;
using AndroidX.AppCompat.App;
using AndroidX.DrawerLayout.Widget;

namespace GNow_Android
{
    public partial class MainActivity
    {
        private class MainActionBarDrawerToggle : ActionBarDrawerToggle
        {
            private readonly MainActivity owner;

            public MainActionBarDrawerToggle(MainActivity activity, DrawerLayout layout, int openRes, int closeRes)
              : base(activity, layout, openRes, closeRes)
            {
                owner = activity;
            }

            public override void OnDrawerSlide(View drawerView, float slideOffset)
            {
                try
                {
                    base.OnDrawerSlide(drawerView, slideOffset);
                    if (slideOffset == 0)
                    {
                        owner.MainActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
                    }
                    else
                    {
                        owner.MainActionBar.SetHomeAsUpIndicator(Resource.Drawable.arrow_left);
                    }
                }
                catch { }
            }
        }
    }
}