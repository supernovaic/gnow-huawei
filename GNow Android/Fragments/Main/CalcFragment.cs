﻿using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Google.Android.Material.TextField;
using Supernova.Database.DAO;
using Supernova.Database.Entities;
using Supernova.Enums;
using System;
using System.Collections.Generic;

namespace GNow_Android
{
    public class CalcFragment : AndroidX.Fragment.App.Fragment
    {
        private AutoCompleteTextView spinnerUnitsCalc;
        private TextView lblResult;
        private ImageView imgCalculate;
        private TextInputEditText txtLatitude;
        private TextInputEditText txtAltitude;
        private Button formulaButton;
        private TextInputLayout lblAltitudeHint;
        private Profile profile;

        private List<string> uValues;

        public static AndroidX.Fragment.App.Fragment NewInstance()
        {
            return new CalcFragment();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.Calc, container, false);

            uValues = new List<string>
            {
                GetString(Resource.String.hintUnitsMetric),
                GetString(Resource.String.hintUnitsEmperial)
            };

            txtLatitude = view.FindViewById<TextInputEditText>(Resource.Id.txtLatitude);
            txtAltitude = view.FindViewById<TextInputEditText>(Resource.Id.txtAltitude);
            lblResult = view.FindViewById<TextView>(Resource.Id.lblResult);
            spinnerUnitsCalc = view.FindViewById<AutoCompleteTextView>(Resource.Id.spinnerUnitsCalc);
            imgCalculate = view.FindViewById<ImageView>(Resource.Id.imgCalculate);
            formulaButton = view.FindViewById<Button>(Resource.Id.formulaButton);
            lblAltitudeHint = view.FindViewById<TextInputLayout>(Resource.Id.lblAltitudeHint);

            formulaButton.Click += FormulaLink_Click;

            var adapter = new ArrayAdapter<string>(Activity, Resource.Layout.DropdownMenuPopupItem, uValues);

            spinnerUnitsCalc.Adapter = adapter;

            spinnerUnitsCalc.SetText(uValues[0], false);

            txtLatitude.TextChanged += TxtAltLat_TextChanged;
            txtAltitude.TextChanged += TxtAltLat_TextChanged;
            spinnerUnitsCalc.ItemClick += (sender, args) =>
            {
                lblAltitudeHint.Hint = (Units)args.Position == Units.Feet ? GetString(Resource.String.lblAltitudeFT) : GetString(Resource.String.lblAltitudeM);

                if (!string.IsNullOrEmpty(txtAltitude.Text) && !string.IsNullOrEmpty(txtLatitude.Text))
                {
                    CalculateGravity(double.Parse(txtLatitude.Text), double.Parse(txtAltitude.Text), (Units)args.Position);
                }
                else
                {
                    imgCalculate.Visibility = ViewStates.Visible;
                    lblResult.Visibility = ViewStates.Gone;
                }
            };

            GetProfile();

            return view;
        }

        private void FormulaLink_Click(object sender, EventArgs e)
        {
            View dialogView = LayoutInflater.Inflate(Resource.Layout.HelpFormula, null);
            using var Dialog = new Android.App.AlertDialog.Builder(Activity);
            Dialog.SetView(dialogView);
            Dialog.Show();
        }

        private async void GetProfile()
        {
            try
            {
                profile = await new ProfileDAO().GetProfile();

                spinnerUnitsCalc.SetText(uValues[(int)profile.SelectedUnits], false);

                lblAltitudeHint.Hint = profile.SelectedUnits == Units.Feet ? GetString(Resource.String.lblAltitudeFT) : GetString(Resource.String.lblAltitudeM);
            }
            catch { }
        }

        private int GetItemPosition(List<string> options, string chosen)
        {
            return Array.IndexOf(options.ToArray(), chosen);
        }

        private void TxtAltLat_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAltitude.Text) && !string.IsNullOrEmpty(txtLatitude.Text))
            {
                CalculateGravity(double.Parse(txtAltitude.Text), double.Parse(txtLatitude.Text), (Units)GetItemPosition(uValues, spinnerUnitsCalc.Text));
            }
            else
            {
                imgCalculate.Visibility = ViewStates.Visible;
                lblResult.Visibility = ViewStates.Gone;
            }
        }

        private void CalculateGravity(double altitude, double latitude, Units units)
        {
            imgCalculate.Visibility = ViewStates.Gone;
            lblResult.Visibility = ViewStates.Visible;

            var gValue = new Supernova.Core.Gravity(latitude, 0, altitude);

            if (units == Units.Feet)
            {
                altitude = gValue.ChangeToMetres(altitude);
                gValue = new Supernova.Core.Gravity(latitude, 0, altitude);
            }

            var result = gValue.GetGravity();

            lblResult.Text = result switch
            {
                0 => GetString(Resource.String.errLatitude),
                1 => GetString(Resource.String.errAltitudeAbove),
                2 => GetString(Resource.String.errAltitudeBelow),
                _ => (units == Units.Feet) ? lblResult.Text = $"{Math.Round(gValue.ChangeToFeet(result), profile.CalcDecimal)} ft/s²" : lblResult.Text = $"{Math.Round(result, profile.CalcDecimal)} m/s²",
            };
        }
    }
}