using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using GNow_Android;

namespace Supernova.Model.Generic
{
    public class DrawerAdapter : BaseAdapter<DrawerData>
    {
        private readonly List<DrawerData> data;

        public DrawerAdapter(List<DrawerData> data)
        {
            this.data = data;
        }

        public override DrawerData this[int position]
        {
            get
            {
                return data[position];
            }
        }

        public override int Count
        {
            get
            {
                return data.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }


        private class DrawerViewHolder : Java.Lang.Object
        {
            public TextView LblTitle { get; set; }
            public ImageView ImgThumbnail { get; set; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var inflater = LayoutInflater.From(parent.Context);
            DrawerViewHolder holder = null;
            var view = convertView;

            if (view != null)
            {
                holder = view.Tag as DrawerViewHolder;
            }

            if (holder == null)
            {
                holder = new DrawerViewHolder();
                view = inflater.Inflate(Resource.Layout.DrawerListItem, parent, false);
                holder.LblTitle = view.FindViewById<TextView>(Resource.Id.Title);
                holder.ImgThumbnail = view.FindViewById<ImageView>(Resource.Id.Thumbnail);
                view.Tag = holder;
            }

            holder.LblTitle.Text = data[position].name;

            try
            {
                holder.ImgThumbnail.SetImageDrawable(data[position].image);
            }
            catch
            {
                holder.ImgThumbnail = null;
            }

            return view;
        }
    }
}
