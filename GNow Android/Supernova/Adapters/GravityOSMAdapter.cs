using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using GNow_Android;
using Android.Text;
using Android.OS;

namespace Supernova.Model.Generic
{
    public class GravityOSMAdapter : BaseAdapter<GravityOSM>
    {
        private readonly List<GravityOSM> data;

        public GravityOSMAdapter(List<GravityOSM> data)
        {
            this.data = data;
        }

        public override GravityOSM this[int position]
        {
            get
            {
                return data[position];
            }
        }

        public override int Count
        {
            get
            {
                return data.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }


        private class GravityOSViewHolder : Java.Lang.Object
        {
            public TextView LblAddress { get; set; }
            public TextView LblGravity { get; set; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var inflater = LayoutInflater.From(parent.Context);
            GravityOSViewHolder holder = null;
            var view = convertView;

            if (view != null)
            {
                holder = view.Tag as GravityOSViewHolder;
            }

            if (holder == null)
            {
                holder = new GravityOSViewHolder();
                view = inflater.Inflate(Resource.Layout.OSMList, parent, false);
                holder.LblAddress = view.FindViewById<TextView>(Resource.Id.lblAddress);
                holder.LblGravity = view.FindViewById<TextView>(Resource.Id.lblGravity);
                view.Tag = holder;
            }

			if (Build.VERSION.SdkInt >= BuildVersionCodes.N)
            {
				holder.LblAddress.TextFormatted = Html.FromHtml($"<b>{data[position].addressKeyword}</b>{data[position].address}", FromHtmlOptions.ModeCompact);
				holder.LblGravity.TextFormatted = Html.FromHtml($"<b>{data[position].gravityKeyword}</b>{data[position].gravity}", FromHtmlOptions.ModeCompact);
            }
            else
            {
#pragma warning disable CS0618
				holder.LblAddress.TextFormatted = Html.FromHtml($"<b>{data[position].addressKeyword}</b>{data[position].address}");
#pragma warning disable CS0618
				holder.LblGravity.TextFormatted = Html.FromHtml($"<b>{data[position].gravityKeyword}</b>{data[position].gravity}");
            }

            return view;
        }
    }
}
