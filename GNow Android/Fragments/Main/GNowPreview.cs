﻿using System;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using AndroidX.Fragment.App;

namespace GNow_Android
{
    public partial class GNowPreview : Fragment
    {
        private const string GNOW_URL = "https://fanmixco.github.io/gravitynow-angular?isApp=true&isMetric={0}";

        private WebView webView;
        private ProgressBar indeterminateBar;
        private Button refreshLink;
        private Context context;

        private bool isMetric;

        public static Fragment NewInstance()
        {
            return new GNowPreview();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.Previewer, container, false);

            context = view.Context;

            webView = view.FindViewById<WebView>(Resource.Id.webView);
            indeterminateBar = view.FindViewById<ProgressBar>(Resource.Id.indeterminateBar);

#if DEBUG
            WebView.SetWebContentsDebuggingEnabled(true);
#endif

            webView.Settings.JavaScriptEnabled = true;

            webView.Settings.DomStorageEnabled = true;

            webView.SetWebViewClient(new WebClient(indeterminateBar, webView));

            refreshLink = view.FindViewById<Button>(Resource.Id.refreshLink);

            refreshLink.Click += RefreshLink_Click;

            GetProfile();

            return view;
        }

        private async void GetProfile()
        {
            try
            {
                var profile = await new Supernova.Database.DAO.ProfileDAO().GetProfile();

                isMetric = profile.SelectedUnits == Supernova.Enums.Units.Meters;

                RefreshView(isMetric);
            }
            catch
            {
                RefreshView(true);
            }
        }

        private void RefreshLink_Click(object sender, EventArgs e)
        {
            RefreshView(isMetric);
        }

        private void RefreshView(bool isMetric)
        {
            indeterminateBar.Visibility = ViewStates.Visible;
            refreshLink.Visibility = ViewStates.Gone;
            if (!CheckInternetState())
            {
                Toast.MakeText(context, context.GetString(Resource.String.LblOnline), ToastLength.Long).Show();
                webView.Visibility = ViewStates.Gone;
                refreshLink.Visibility = ViewStates.Visible;
            }
            else
            {
                webView.LoadUrl(string.Format(GNOW_URL, isMetric));
            }
        }

        private bool CheckInternetState()
        {
            return Xamarin.Essentials.Connectivity.NetworkAccess == Xamarin.Essentials.NetworkAccess.Internet;
        }
    }
}