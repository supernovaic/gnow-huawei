﻿using SQLite;
using Supernova.Database.Entities;
using System;
using System.IO;
using Supernova.Enums;
using Supernova.Database.DAO;
using Android.Content;
using Java.IO;
using System.Threading.Tasks;

namespace Supernova.Database
{
    public class DatabaseManagement
    {
        private const string DB_NAME = "gnow.db";

        protected static SQLiteAsyncConnection ConnectionDb()
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var conn = new SQLiteAsyncConnection(Path.Combine(folder, DB_NAME), true);
            return conn;
        }

        public static void CopyDataBase(Context context)
        {
            using var file = new Java.IO.File(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), DB_NAME));

            if (!file.Exists())
            {
                var istream = context.Assets.Open(DB_NAME);
                var outDB = new FileOutputStream(file);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = istream.Read(buffer)) > 0)
                {
                    outDB.Write(buffer, 0, length);
                }
                istream.Close();
                outDB.Flush();
                outDB.Close();
            }
        }

        public async static Task<Profile> AddProfile()
        {
            ProfileDAO.Insert(new Profile() { Email = "", Sync = false, CalcDecimal = 8, SelectedUnits = Units.Meters, NDecimal = 3, Name = "" });
            return await new ProfileDAO().GetProfile();
        }
    }
}
