﻿using System.Collections.Generic;
using Android.Graphics;
using Android.OS;
using Android.Text;
using Android.Views;
using Android.Widget;
using GNow_Android;

namespace Supernova.Model.Generic
{
    public class CompareAllAdapter : BaseAdapter<DrawerData>
    {
        private readonly List<DrawerData> data;

        public CompareAllAdapter(List<DrawerData> data)
        {
            this.data = data;
        }

        public override DrawerData this[int position]
        {
            get
            {
                return data[position];
            }
        }

        public override int Count
        {
            get
            {
                return data.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        private class ViewHolderCompareAll : Java.Lang.Object
        {
            public ImageView ImgListCObject { get; set; }
            public TextView LblData { get; set; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var inflater = LayoutInflater.From(parent.Context);
            ViewHolderCompareAll holder = null;
            var view = convertView;

            if (view != null)
            {
                holder = view.Tag as ViewHolderCompareAll;
            }

            if (holder == null)
            {
                holder = new ViewHolderCompareAll();
                view = inflater.Inflate(Resource.Layout.ItemCompareAll, parent, false);
                holder.LblData = view.FindViewById<TextView>(Resource.Id.lblData);
                holder.ImgListCObject = view.FindViewById<ImageView>(Resource.Id.imgListCObject);
                view.Tag = holder;
            }

            if (Build.VERSION.SdkInt >= BuildVersionCodes.N)
            {
                holder.LblData.TextFormatted = Html.FromHtml(data[position].name, FromHtmlOptions.ModeCompact);
            }
            else
            {
#pragma warning disable CS0618 // Type or member is obsolete
                holder.LblData.TextFormatted = Html.FromHtml(data[position].name);
#pragma warning restore CS0618 // Type or member is obsolete
            }

            holder.LblData.SetTextColor(Color.ParseColor(data[position].color));

            try
            {
                holder.ImgListCObject.SetImageDrawable(data[position].image);
            }
            catch
            {
                holder.ImgListCObject = null;
            }

            return view;
        }
    }
}
